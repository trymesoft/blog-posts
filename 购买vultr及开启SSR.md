#### 1. 首先官网注册账号

[点我注册账号](https://www.vultr.com/?ref=7405465)



---

#### 2. 充值

vultr支持支付宝及PayPal和信用卡支付。

<img src="https://wx2.sinaimg.cn/large/007jb4T5ly1fxh2lftpn5j31f512gq7h.jpg" align="center" alt="image">


---

#### 3. 选择属性

- 地区

<img src="https://wx2.sinaimg.cn/large/007jb4T5ly1fxh2ma9jp1j31p10xcjxl.jpg" align="center" alt="image">

- 选择操作系统（建议centos6）

<img src="https://ws1.sinaimg.cn/large/007jb4T5ly1fxh2nd1n86j31oa0lg0w6.jpg" align="center" alt="image">

- 选择配置

<img src="https://ws3.sinaimg.cn/large/007jb4T5ly1fxh2o800xyj31pt0vj7ai.jpg" align="center" alt="image">

- 额外选项（根据需要自定义）

<img src="https://wx1.sinaimg.cn/large/007jb4T5ly1fxh2oz8lj6j312m0vm0v5.jpg" align="center" alt="image">

- 等待安装

<img src="https://wx3.sinaimg.cn/large/007jb4T5ly1fxh2ps7645j31rn0jlgow.jpg" align="center" alt="image">


---

#### 4. 登录VPS

- 获得IP及密码，登录远程VPS

<img src="https://wx3.sinaimg.cn/large/007jb4T5ly1fxh2qlboaaj31qw0ttwkm.jpg" align="center" alt="image">

- 安装脚本（逗比提供）

`wget -N --no-check-certificate https://raw.githubusercontent.com/ToyoDAdoubi/doubi/master/ssr.sh && chmod +x ssr.sh && bash ssr.sh`

- 按提示操作

<img src="https://wx2.sinaimg.cn/large/007jb4T5ly1fxh2rg59zhj30x60wqjw0.jpg" align="center" alt="image">
<img src="https://wx3.sinaimg.cn/large/007jb4T5ly1fxh2sc25lcj31wb10ktjs.jpg" align="center" alt="image">


**最后可以使用SSR客户端进行连接测试。**