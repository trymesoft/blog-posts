> 从MyEclipse转IDEA已经有一段时间了，深深体会到一个强大的IDE对开发者来说是多么的重要，IDEA拥有强大的快捷键及酷炫的界面风格，花了点时间勉强能上手

> 开发，在此记录一下maven项目的创建。

---

#### 1. 创建新项目，并勾选以下选项



<img src="https://ws3.sinaimg.cn/large/007jb4T5ly1fxguw9q3q5j30k70g8mzd.jpg" align="center" alt="image">




---

#### 2. 填写GroupId和ArtifactId

> GroupId：定义了项目属于哪个组，一般来说这个通常和公司或组织关联；

> ArtifactId：定义了当前Maven项目在组中的唯一id  



<img src="https://wx3.sinaimg.cn/large/007jb4T5ly1fxguz2vp68j30k70g3aac.jpg" align="center" alt="image">



---

#### 3. 配置maven（可以略过，创建完项目再进行更改）

<img src="https://wx4.sinaimg.cn/large/007jb4T5ly1fxgyk76up8j30k70g7js4.jpg" align="center" alt="image">


---

#### 4. 项目创建完成后，每次配置 pom文件都要刷新maven依赖

<img src="https://wx1.sinaimg.cn/large/007jb4T5ly1fxgylc5xq6j30cp09lq36.jpg" align="center" alt="image">




至此，一个新的基于maven管理依赖jar包的web项目创建完毕！