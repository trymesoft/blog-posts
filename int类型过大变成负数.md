> 计算机存储系统使用补码标识，int类型占32位，首位是符号位：0表示正数，1表示负数。正数补码与原码相同负数补码除符号位其余各位与原码取反，然后加1  



日常开发中常用的方法我们一般会抽取出公用的方法，想使用一个操作日期的工具类，本来想用Apache的commons包的DateUtils工具类，但是没有找到想要的方法，转而求其次使用之前同事封装的工具类，结果遇到一个问题。



**需求：给定一个Date对象，找到特定天数后的日期时间**



工具类代码如下：

```java

public static Date datetimeAddDay(Date datetime, int day) {

 datetime.setTime(datetime.getTime() + day * 24 * 60 * 60 * 1000);

 return datetime;

}

```

猛的一看，并没有什么不对，结果运行时日期不对，写个测试类跑了一下，大吃一惊！！！  

测试类代码：

```java

@Test

public void testDatetimeAddDay() {

    Date datetime = new Date();

    System.out.println("当前时间：" + datetime);

    Date datetimeAddDay = DateUtil.datetimeAddDay(datetime, 30);

    System.out.println("30天后：" + datetimeAddDay);

}

```

运行结果：

<img src="https://ws1.sinaimg.cn/large/007jb4T5ly1fxgz4rygw0j308o010aaa.jpg" align="center" alt="image">


这时间还给我倒回去了，原来是工具类的方法加的毫秒数超过int类型最大值了 **2的31次方-1**，符号位成了1，整体变为负数。所以工具类中运算的毫秒数使用long类型即可！！！