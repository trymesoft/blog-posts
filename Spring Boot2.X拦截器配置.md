
> Spring Boot2.0之后的拦截器配置和之前不太一样，但是殊途同归，特此总结一下😝
### Spring Boot2.0之前
Spring Boot2.0之前的拦截器配置如下：
```java
@Configuration
public class InterceptorConfigurerAdapter extends WebMvcConfigurerAdapter {
    @Bean
    RestRequestValidatorInterceptor restRequestValidatorInterceptor() {
        return new RestRequestValidatorInterceptor();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(restRequestValidatorInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/push/test");
    }
}
```
这是没问题的，但是在Spring Boot2.0或更高版本中，WebMvcConfigurerAdapter类已经deprecated：

<img src="https://wx4.sinaimg.cn/large/007jb4T5ly1fxh2tu0galj312s05qwfe.jpg" align="center" alt="image">
虽然过期，但是可以看到WebMvcConfigurerAdapter实现了WebMvcConfigurer接口，所以可以直接自定义类实现WebMvcConfigurer接口，其他不变。
### Spring Boot2.0之后
只需要将之前继承的WebMvcConfigurerAdapter类改为实现WebMvcConfigurer接口。
```java
@Configuration
public class InterceptorConfigurerAdapter implements WebMvcConfigurer {
    @Bean
    RestRequestValidatorInterceptor restRequestValidatorInterceptor() {
        return new RestRequestValidatorInterceptor();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(restRequestValidatorInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/push/test");
    }
}
```