> openfire是一个是一个IM（即时通讯）服务器，使用java编写的XMPP服务器，开发过程稿Intellij IDEA导入openfire源码步骤比较复杂，记录一下。

## 下载源码
从GitHub项目主页[下载](https://github.com/igniterealtime/Openfire)源码或者从官网[下载](https://www.igniterealtime.org/projects/openfire/)。

---
## 导入到IDEA中
### 选择Import Project
<img src="https://ws3.sinaimg.cn/large/007jb4T5ly1fwh4u917jfj30lu0keq3s.jpg" alt="image" style="display:block;margin:0 auto;"/>
一路next即可导入成功。
### 进行构建
注意：由于需要ant相关jar包，可以[下载](https://ant.apache.org/bindownload.cgi)之后，将ant.jar导入到$JAVA_HOME/jre/lib目录下即可。
<img src="https://ws1.sinaimg.cn/large/007jb4T5ly1fwh59sa2qrj31j80os44v.jpg" alt="image" style="display:block;margin:0 auto;">
添加之后进行编译报错：
<img src="https://ws3.sinaimg.cn/large/007jb4T5ly1fwh5amisywj31as0k0acw.jpg" style="display:block;margin:0 auto;">
解决：配置jdk
<img src="https://wx2.sinaimg.cn/large/007jb4T5ly1fwh5e3ra57j31h20ligpd.jpg" alt="image" style="display:block;margin:0 auto;">

配置application
 <img src="https://ws4.sinaimg.cn/large/007jb4T5ly1fwh5fgxstfj31t4188tdw.jpg" alt="image" style="display:block;margin:0 auto;">
### 启动应用
成功启动后进入 http://localhost:9000 页面报错如下：
<img src="https://ws3.sinaimg.cn/large/007jb4T5ly1fwh5gy3gm6j31a00ziwrr.jpg" alt="image" style="display:block;margin:0 auto;">
解决：将$OPENFIRE_HOME/src/resources/jar/admin-sidebar.xml及$OPENFIRE_HOME/resources/i18n下所有文件拷贝到输出目录。
<img src="https://wx4.sinaimg.cn/large/007jb4T5ly1fwh5me9vf7j317m0i0acs.jpg" alt="image" style="display:block;margin:0 auto;">
<img src="https://ws1.sinaimg.cn/large/007jb4T5ly1fwh5mreoutj30yq0k4q6b.jpg" alt="image" style="display:block;margin:0 auto;">
输出目录：
<img src="https://ws1.sinaimg.cn/large/007jb4T5ly1fwh5namvjsj319w0k2q72.jpg" alt="image" style="display:block;margin:0 auto;">
再次启动即可进入初始化配置页面。
### 管理页面配置
根据提示配置即可。
<img src="https://ws4.sinaimg.cn/large/007jb4T5ly1fwh729eol1j31y00yuwj2.jpg" alt="image" style="display:block;margin:0 auto;">
<img src="https://wx2.sinaimg.cn/large/007jb4T5ly1fwh72p0gioj318c0l0whe.jpg" alt="image" style="display:block;margin:0 auto;">
<img src="https://ws4.sinaimg.cn/large/007jb4T5ly1fwh73l1dxmj31hc0p443e.jpg" alt="image" style="display:block;margin:0 auto;">
<img src="https://wx4.sinaimg.cn/large/007jb4T5ly1fwh73xkewoj31w20hiade.jpg" alt="image" style="display:block;margin:0 auto;">

至此，idea导入openfire源码完成！
